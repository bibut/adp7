package tree;

import java.util.ArrayList;

//@SuppressWarnings({ "rawtypes", "unchecked" })
public class ArrayTree<T extends Comparable<T>> extends Tree<T> {

	private ArrayList<Node<T>> treeArrayList;
	private final int ROOTINDEX = 1;
	T preroot;

	public ArrayTree(T root) {
		treeArrayList = new ArrayList<Node<T>>();
		treeArrayList.add(new Node<T>(preroot));
		treeArrayList.add(new Node<T>(root));
	}

	public ArrayTree(T root, int size) {
		treeArrayList = new ArrayList<Node<T>>(size + 1);
		treeArrayList.add(new Node<T>(root));
	}

	@Override
	public void insert(T newData) {
		int index = ROOTINDEX;
		T temp;
		boolean isOccupied = false;
		while (!isOccupied) {

			temp = getData(index);

			if (((T) getLeftNodeValue(temp)) == null) {
				treeArrayList.add(new Node<T>(newData));
				isOccupied = true;

			} else if (((T) getRightNodeValue(temp)) == null) {
				treeArrayList.add(new Node<T>(newData));
				isOccupied = true;
			}
			index++;
		}

	}

	@Override
	public T getLeftNodeValue(T parent) {
		int index = getIndex((T) parent);
		if (index == 0) {
			return null;
		}
		int kIndex = getLeftNodeIndex(index);
		return getData(kIndex);
	}

	@Override
	public T getRightNodeValue(T parent) {
		int index = getIndex((T) parent);
		if (index == 0) {
			return null;
		}
		int kIndex = getRightNodeIndex(index);
		return getData(kIndex);
	}

	public int getLeftNodeIndex(int parentIndex) {
		int kIndex = 0;
		if (parentIndex > 0) {
			kIndex = 2 * parentIndex;
		}
		return kIndex;
	}

	public int getRightNodeIndex(int parentIndex) {
		int kIndex = 0;
		if (parentIndex > 0) {
			kIndex = 2 * parentIndex + 1;
		}
		return kIndex;
	}

	/**
	 * 
	 * @return the index-value of the node
	 */
	public int getIndex(T data) {
		int index = ROOTINDEX;
		boolean found = false;
		while (index <= treeArrayList.size() && !found) {
			if (data == null || getData(index) == null) {
				return 1;
			}
			int searchedNode = data.compareTo(getData(index));

			if (searchedNode == 0) {
				found = true;
			}
			if (searchedNode < 0) {
				index = getLeftNodeIndex(index);
			} else {
				index = getRightNodeIndex(index);
			}
		}
		return index;
	}

	public T getData(int index) {
		T data = null;
		if (index > 0 && index < treeArrayList.size()) {
			data = (T) treeArrayList.get(index).getValue();
		}
		return data;

	}

	public void printInorder() {
		System.out.println("Inorder: ");
		inorder(ROOTINDEX);
		System.out.println("");
	}

	public void inorder(int tempIndex) {

		if (getData(tempIndex) == null) {
			return;
		}

		// first recur left subtree
		inorder(getLeftNodeIndex(tempIndex));

		// print data of node
		System.out.print(getData(tempIndex) + " ");

		// now recur right subtree
		inorder(getRightNodeIndex(tempIndex));

	}

	public void printPreorder() {
		System.out.println("Preorder: ");
		preorder(ROOTINDEX);
		System.out.println("");
	}

	public void preorder(int tempIndex) {

		if (getData(tempIndex) == null) {
			return;
		}

		// print data of node
		System.out.print(getData(tempIndex) + " ");

		// first recur left subtree
		preorder(getLeftNodeIndex(tempIndex));

		// now recur right subtree
		preorder(getRightNodeIndex(tempIndex));

	}

	public void printPostorder() {
		System.out.println("Postorder: ");
		postorder(ROOTINDEX);
		System.out.println("");
	}

	public void postorder(int tempIndex) {

		if (getData(tempIndex) == null) {
			return;
		}

		// first recur left subtree
		postorder(getLeftNodeIndex(tempIndex));

		// now recur right subtree
		postorder(getRightNodeIndex(tempIndex));

		// print data of node
		System.out.print(getData(tempIndex) + " ");
	}

	public static void main(String[] args) {

		ArrayTree ersterbaum = new ArrayTree("*");
		ersterbaum.insert("+");
		ersterbaum.insert("/");
		ersterbaum.insert("*");
		ersterbaum.insert("4");
		ersterbaum.insert("5");
		ersterbaum.insert("7");
		ersterbaum.insert("3");
		ersterbaum.insert("12");

		ersterbaum.printInorder();
		ersterbaum.printPreorder();
		ersterbaum.printPostorder();
	}

}
