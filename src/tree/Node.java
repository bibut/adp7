package tree;

/**
 * 
 * @author Ivy
 *
 * @param <T>
 *            Object, that represents a node (generic)
 */
public class Node<T extends Comparable<T>> {

	/**
	 * the data the node contains
	 */
	private T data;
	/**
	 * node counter for tree
	 */
	private int index = 1;

	/**
	 * Pointer to the left-son-node
	 */
	private Node<T> leftSon;
	/**
	 * Pointer to the right-son-node
	 */
	private Node<T> rightSon;

	/**
	 * 
	 * @param data
	 *            the data-value to insert
	 * 
	 * 
	 */
	public Node(T data) {
		this.setValue(data);
		this.setIndex(index);
		index++;
	}

	/**
	 * 
	 * @param data
	 *            the data value to insert
	 * @param index
	 *            the tree-index where the data should be placed
	 */
	public Node(T data, int index) {
		this.setValue(data);
		this.setIndex(index);
	}

	/**
	 * 
	 * @return returns the node-index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * 
	 * @param index
	 *            the index-value of the node
	 * 
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * 
	 * @return the data-value of the node
	 */
	public T getValue() {
		return data;
	}

	/**
	 * 
	 * @param value
	 *            the value to insert into the node
	 * 
	 */
	public void setValue(T value) {
		this.data = value;
	}

	/**
	 * 
	 * @return returns the left node
	 */
	public Node<T> getLeftSon() {
		return leftSon;
	}

	/**
	 * 
	 * @param leftSon
	 *            the left son-node
	 * 
	 */
	public void setLeftSon(Node<T> leftSon) {
		this.leftSon = leftSon;
	}

	/**
	 * 
	 * @return returns the right node
	 */
	public Node<T> getRightSon() {
		return rightSon;
	}

	/**
	 * 
	 * @param rightSon
	 *            the right son-node
	 * 
	 */
	public void setRightSon(Node<T> rightSon) {
		this.rightSon = rightSon;
	}

}
