package tree;

/**
 * abstract tree-class
 * 
 * @author Ivy
 *
 * @param <T>
 *            Type of the generic tree
 */
public abstract class Tree<T extends Comparable<T>> {

	/**
	 * 
	 * @param newNode
	 *            Node to insert into the next free tree-spot
	 */
	public abstract void insert(T newData);

	/**
	 * Print the tree in inorder (inorder(k1), k, inorder(k2))
	 */
	public abstract void printInorder();

	/**
	 * Print the tree in preorder (k, preorder(k1), preorder(k2))
	 */
	public abstract void printPreorder();

	/**
	 * Print the tree in postorder (postorder(k1), postorder(k2), k)
	 */
	public abstract void printPostorder();

	/**
	 * 
	 * @param parent
	 *            The parent of the left node
	 * @return the data of left-child-node
	 * 
	 */
	public abstract T getLeftNodeValue(T parent);

	/**
	 * 
	 * @param parent
	 *            the parent of the right node
	 *
	 * @return the data of right-child-node
	 * 
	 */
	public abstract T getRightNodeValue(T parent);

}
