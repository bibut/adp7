package tree;

import java.util.LinkedList;

//@SuppressWarnings({ "rawtypes", "unchecked" })
public class LinkedTree<T extends Comparable <T>> extends Tree<T> {

	private LinkedList<Node<T>> treeLinkedList = new LinkedList<Node<T>>();
	private final int ROOTINDEX = 1;
	T preroot;
	

	public LinkedTree(T root) {
		treeLinkedList.add(new Node<T>(preroot));
		treeLinkedList.add(new Node<T>(root));
	}

	@Override
	public void insert(T newData) {

		Node<T> newNode = new Node<T>(newData, treeLinkedList.size());
		insertNode(this.treeLinkedList.get(ROOTINDEX), newNode);
	}

	public void insertNode(Node<T> node, Node<T> newNode) {

		if (treeLinkedList.size() % 2 == 0) {
			if (node.getLeftSon() == null) {
				node.setLeftSon(newNode);
				treeLinkedList.add(newNode);
				return;
			} else {
				insertNode(node.getLeftSon(), newNode);

			}

		} else {
			if (node.getRightSon() == null) {
				node.setRightSon(newNode);
				treeLinkedList.add(newNode);
				return;
			} else {
				insertNode(node.getRightSon(), newNode);
			}
		}

	}

	@Override
	public T getLeftNodeValue(T parent) {
		int i = ROOTINDEX;
		while (!treeLinkedList.get(i).getValue().equals(parent)) {
			i++;
		}
		return treeLinkedList.get(i).getLeftSon().getValue();
	}

	@Override
	public T getRightNodeValue(T parent) {
		int i = ROOTINDEX;
		while (!treeLinkedList.get(i).getValue().equals(parent)) {
			i++;
		}
		return treeLinkedList.get(i).getRightSon().getValue();
	}

	public void printInorder() {
		System.out.println("Inorder: ");
		inorder(ROOTINDEX);
		System.out.println("");

	}

	public void inorder(int tempIndex) {

		if (treeLinkedList.get(tempIndex).getValue() == null) {
			return;
		}
		// first recur left subtree
		try {
			inorder(tempIndex * 2);
		} catch (Exception e) {
		}

		// print data of node

		System.out.print(treeLinkedList.get(tempIndex).getValue() + " ");

		// now recur right subtree
		try {
			inorder((tempIndex * 2 + 1));
		} catch (Exception e) {
		}

	}

	public void printPreorder() {
		System.out.println("Preorder: ");
		preorder(ROOTINDEX);
		System.out.println("");
	}

	public void preorder(int tempIndex) {

		if (treeLinkedList.get(tempIndex).getValue() == null) {
			return;
		}

		// print data of node
		System.out.print(treeLinkedList.get(tempIndex).getValue() + " ");

		// first recur left subtree
		try {
			preorder(tempIndex * 2);
		} catch (Exception e) {
		}

		// noew right subtree
		try {
			preorder((tempIndex * 2 + 1));
		} catch (Exception e) {
		}

	}

	public void printPostorder() {
		System.out.println("Postorder: ");
		postorder(ROOTINDEX);
		System.out.println("");
	}

	public void postorder(int tempIndex) {

		if (treeLinkedList.get(tempIndex).getValue() == null) {
			return;
		}

		// first recur left subtree
		try {
			postorder(tempIndex * 2);
		} catch (Exception e) {
		}

		// noew right subtree
		try {
			postorder((tempIndex * 2 + 1));
		} catch (Exception e) {
		}

		// print data of node
		System.out.print(treeLinkedList.get(tempIndex).getValue() + " ");

	}

	public static void main(String[] args) {

		LinkedTree ersterbaum = new LinkedTree("*");
		ersterbaum.insert("+");
		ersterbaum.insert("/");
		ersterbaum.insert("*");
		ersterbaum.insert("4");
		ersterbaum.insert("5");
		ersterbaum.insert("7");
		ersterbaum.insert("3");
		ersterbaum.insert("12");
		ersterbaum.printInorder();
		ersterbaum.printPreorder();
		ersterbaum.printPostorder();
	}

}
